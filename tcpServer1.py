#!/usr/bin/python
# a simple tcp server

# IP at which the server listens
server_ip = '127.0.0.1'
# Port it listens at
server_port = 12345

import socket,os
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((server_ip, server_port))
sock.listen(5)
while True:
    connection,address = sock.accept()
    buf = connection.recv(1024)
    print(buf)
    if buf.strip() == "ping":
        connection.send("pong\n")
    #connection.send(buf)
    connection.close()

