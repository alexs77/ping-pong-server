# ping-pong Server #

Ein Server, der "PONG" ausgibt, wenn man "PING" hinsendet.

## Todo ##

- Was ist zu tun, damit der Server "PONG" zurückgibt?
- Wie erreichen, das der Server (`tcpServer1.py`) auch aus dem Internet
  angesprochen werden kann?

## Links ##

- <https://gist.github.com/mike-zhang/3803593>
